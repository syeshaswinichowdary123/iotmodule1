# IoT Basics

## What is IoT?
   In Simple means IoT is nothing connecting things to internet.
   IoT is used in various sectors like: Home IoT, Industrial IoT etc.,

## Industrial IoT 
It is nothing but connecting industrial things to internet.

Before going with industrial IoT let's get with what industries are and what they have
## Industrial Revolution
![Revolution](extras/ir1.png)

This is how industrial revolution had happend Industrial 4.0 is evoluving, but still few industries where still stuck to Industrial 3.0. To get familiar let's go in depth.

### Industry 3.0
 Here Data is stored in databases and represented in excels. 

![Rvolution 3.0](extras/ir2.png)

this is the flow of industrial revolution 3.0

- Field Device: Sensors, Actuators.., these collect the data.

- Control Device:Pc's,Plc's.., they control the field devices and tell what actiond should be performed.

- Station: Set of Field and Control Devices together is called Station.
- Work Centers: Set of Stations are called Work Centers are Factories.
- Enterprises: Set of stations and Work Centers are Enterprises.

#### Industry 3.0 Architecture
![revolution 3.0](extras/ir3.png)
- Sensors and Actuators are Field Devices that collect data at various sectors and send to Control devices through field bus.
- PLC's are Control Devices which collect all the data and send it to SCADA and ERP systems for storing the data.

- SCADA HISTORIAN is a Work Center Where as ERP MES is a Enterprises.These are the softwares that store data and arrange data.
#### Industry 3.0 communication protocols
Protocols are nothing but of rules.There are few rules to communicate between the devices(field,control).         
**Communication Protocols**

![protocols](extras/ir4.png)

Modbus is most prominent among them.
### Industry 4.0(IoT)
Industry 4.0 is Industry 3.0 devices connected to the Internet, which we call as IoT.
When we connect devices to internet what happens is they sends data to us.

![industry 4.0](extras/ir5.png)

With that information we can perform any operations and can stay connected ourself with devices no matter where we are.

![industry data](extras/ir6.png)
#### Industry 4.0 Architecture
![architecture](extras/ir7.png)

Here two extra blocks were added :EDGE and CLOUD.

EDGE acts like a translator which speaks with the controllers and send data to cloud through protocols so cloud can understand it.
#### Industry 4.0 communication protocols
These are the Protocols through which cloud can understand.

![protocols](extras/ir8.png)

**There few problems to switch to Industry 4.0 they are:**
- Cost:Industry 4.0 devices because they are Expensive
- Downtime:it takes time, so no one will be willing to shutdown their factories.
- Reliability: There trustworthy is not proven.

**What is the Solution?**

Getting data from Industry 3.0 devices without changes to the original device. And then send the data to the Cloud using Industry 4.0 devices.

**How do we get data from Industry 3.0 device ?**

Simply we need to design a translator which converts data from Modbus protocols to MQTT protocols.

**Challenges we face during Conversion**
- Expensive Hardware: The hardware is costly.
- Lack of documentaion: The proper documentation is not done. Only the manufacturer knows what happens inside. And the Properitary PLC protocols.

**How to convert ?**

There is a library that helps to get data from Industry 3.0 devices and send to Industry 4.0 cloud.It runs on small embedded boards(Raspberry pi). It is cost effective.

![library](extras/ir9.png)

#### What next?
- Once we sent the data, the data is stored in **IoT TSDB tools >(Grafana,Thingsboard)** means data is stored with respect to time.
- We can analyse our data on IoT platforms(Thingsboard,Google IoT).
- We can get alerts and update through messages using Zaiper and Twilio.
